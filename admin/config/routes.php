<?php
return array(
    "admin"                         => "home/index",
    "admin/login"                   => "home/login",
    "admin/logout"                  => "home/logout",
    "admin/page404"                 => "home/page404",
    "admin/categories"              => "category/list",
    "admin/category/edit/([0-9]+)"  => "category/edit/$1",
    "admin/category/create"         => "category/create",
    "admin/category/([0-9]+)"       => "category/view/$1",
    "admin/category/submit"         => "category/submit",
    
    "admin/products"                => "product/list",
    "admin/products/([0-9]+)"        => "product/list/$1",
    "admin/product/edit/([0-9]+)"   => "product/edit/$1",
    "admin/product/add"             => "product/add",
    "admin/product/([0-9]+)"        => "product/view/$1",
    "admin/product/submit"          => "product/submit",


    "admin/photos"                => "photos/list",
    "admin/photos/add"            => "photos/add",
    "admin/photos/submit"         => "photos/submit",
    "admin/photos/edit/([0-9]+)"  => "photos/edit/$1",
    "admin/photos/remove/([0-9]+)"=> "photos/remove/$1",

    "admin/audios"                => "audios/list",
    "admin/audios/add"            => "audios/add",
    "admin/audios/submit"         => "audios/submit",
    "admin/audios/edit/([0-9]+)"  => "audios/edit/$1",
    "admin/audios/remove/([0-9]+)"=> "audios/remove/$1",
);
