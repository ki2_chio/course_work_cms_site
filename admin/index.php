<?php

    define("ROOT", __DIR__);
    
    include("../config/config.php");
    include(ROOT . "/core/autoload.php");

    (new AdminApplication())->run();