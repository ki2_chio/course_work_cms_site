<?php

class ProductModel extends Product 
{   
    public function update() {
        $sql = "UPDATE product SET "
                . "name=?, "
                . "description=?, "
                . "category_id=?, "
                . "sku=?, "
                . "price=?, "
                . "status=?,"
                . "url=?, "
                . "meta_description=?, "
                . "meta_keyword=? "
                . "WHERE product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->category_id,
            $this->sku,
            $this->price,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->product_id,
        ]);
        return $this->product_id;
    }

    public function create() {
        $sql = "INSERT INTO product("
                . "category_id, "
                . "name, "
                . "description, "
                . "url, "
                . "status, "
                . "meta_description, "
                . "meta_keyword) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->category_id,
            $this->name,
            $this->description,
            $this->url,
            $this->status,
            $this->meta_description,
            $this->meta_keyword
        ]);
        return DataBase::handler()->lastInsertId();
    }
    
    public static function changeStatus($status) 
    {
        if (isset($_POST['product']) && !empty($_POST['product'])) {
            $products = $_POST['product'];
            $str = join(", ", $products);
            $sql = "UPDATE product SET status=" . (int)$status . " WHERE product_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
  
}
