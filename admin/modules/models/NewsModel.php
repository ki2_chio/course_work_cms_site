<?php

class PhotoModel extends Photos
{   
    public function update() {
        $sql = "UPDATE photos SET "
                . "name=?, "
                . "f_name=?, "
                . "WHERE photo_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->f_name,
            $this->photo_id,
        ]);
        return $this->photo_id;
    }

    public function create() {
        $sql = "INSERT INTO photos("
                . "name, "
                . "f_name)"
                . "VALUES (?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->f_name
        ]);
        return DataBase::handler()->lastInsertId();
    }
    

}
