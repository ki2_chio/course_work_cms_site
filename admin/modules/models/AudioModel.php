<?php

class AudioModel extends Audios
{   
    public function update() {
        $sql = "UPDATE audios SET "
                . "name=?, "
                . "f_name=?, "
                . "WHERE audio_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->f_name,
            $this->audio_id,
        ]);
        return $this->audio_id;
    }

    public function create() {
        $sql = "INSERT INTO audios("
                . "name, "
                . "f_name)"
                . "VALUES (?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->f_name
        ]);
        return DataBase::handler()->lastInsertId();
    }
}
