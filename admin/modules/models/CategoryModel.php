<?php
class CategoryModel extends Category
{

    public function updateCategoryById() {
        $sql = "UPDATE category SET name=?, "
                . "description=?, "
                . "status=?,"
                . "url=?, "
                . "meta_description=?, "
                . "meta_keywords=? "
                . "WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keywords,
            $this->category_id,
        ]);
        return $this->category_id;
    }
    
    public function createCategory() {
        $sql = "INSERT INTO category("
                . "name, "
                . "description, "
                . "status, "
                . "url, "
                . "meta_description, "
                . "meta_keywords) "
                . "VALUES (?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keywords
        ]);
        return DataBase::handler()->lastInsertId();
    }
       
    public static function changeActiveStatusCategory($status) 
    {
        if (isset($_POST['category']) && !empty($_POST['category'])) {
            $categorys = $_POST['category'];
            $str = join(", ", $categorys);
            $sql = "UPDATE categorys SET is_active=" . (int)$status . " WHERE category_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
}
