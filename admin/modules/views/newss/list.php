<form action="/admin/photos/submit" method="post">
    <div class="list-group-photo">
        <?php foreach($photos as $key => $photo): ?>
            <div class="list-group-item-photo clearfix">
                <input type="checkbox" name="photo[]" value="<?= $photo['photo_id']; ?>">
                <img src="/uploads/photos/<?= $photo['f_name']; ?>" height="50px;">
                <?= $photo['title']; ?>
                <a href="/admin/photos/remove/<?= $photo['name']; ?>" class="btn btn-danger pull-right">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>
                <a href="/admin/photos/edit/<?= $photo['photo_id']; ?>" class="btn btn-default pull-right">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </a>
            </div>
        <?php endforeach;?>
    </div>
</form>