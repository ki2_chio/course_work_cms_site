<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3">   
        <div class="list-group">
            <span class="list-group-item list-group-item-success">Категорії</span>
            <?php foreach($categories as $key => $category): ?>
                <a class="list-group-item" href="/admin/products/<?= $category['category_id']; ?>">
                    <span><?= $category["name"]; ?></span>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    
    <div class="col-xs-12 col-md-8 col-lg-9">
        <form action="/admin/product/submit" method="post">
            <div class="list-group">
                <?php foreach($products as $key => $product): ?>
                <div class="list-group-item clearfix">
                    <input type="checkbox" name="product[]" value="<?= $product['product_id']; ?>">
                    <span class="<?php if (!$product['status']): ?>bg-danger<?php endif; ?>"><?= $product['name']; ?></span>
                    <a href="/admin/product/edit/<?= $product['product_id']; ?>" class="btn btn-default pull-right">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </a>
                </div>
                <?php endforeach;?>
            </div>
            <div class="form-group">
                <label for="form-action">Виберіть дію</label>
                <select name="action" id="form-action" class="form-control">
                    <option value="deactivate">Деактивувати</option>
                    <option value="activate">Активувати</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Виконати" class="btn btn-default">
            </div>
        </form>
    </div>
</div>