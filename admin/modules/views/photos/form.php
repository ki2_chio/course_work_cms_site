<form method='post' action='/admin/photos/submit'  enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="form-photo-id">Photos ID</label>
        <input type='text' class="form-control"  readonly="readonly" name='photo_id' id="form-photo-id" value='<?php echo $photo->photo_id; ?>'>
    </div>
    
    <div class="form-group">
        <label for="form-photo-name">Photos Name</label>
        <input type='text' class="form-control"  name='name' id="form-photo-name" value='<?php echo $photo->name; ?>'>
    </div>

    <div class="form-group">
        <label for="form-photo-file">Файл</label>
        <input type='file' class="form-control"  name='file' id="form-photo-file">
    </div>
    
    <div class="form-group">
        <input type='hidden' name='action' value='<?php echo $action; ?>'>
        <input type='submit' class="btn btn-default">
    </div>
</form>