<form method='post' action='/admin/audios/submit'  enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="form-audio-id">Audio ID</label>
        <input type='text' class="form-control"  readonly="readonly" name='audio_id' id="form-audio-id" value='<?php echo $audio->audio_id; ?>'>
    </div>
    
    <div class="form-group">
        <label for="form-audio-name">Audio Name</label>
        <input type='text' class="form-control"  name='name' id="form-audio-name" value='<?php echo $audio->name; ?>'>
    </div>

    <div class="form-group">
        <label for="form-audio-file">Файл</label>
        <input type='file' class="form-control"  name='file' id="form-audio-file">
    </div>
    
    <div class="form-group">
        <input type='hidden' name='action' value='<?php echo $action; ?>'>
        <input type='submit' class="btn btn-default">
    </div>
</form>