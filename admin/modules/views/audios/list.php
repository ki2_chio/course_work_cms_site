<form action="/admin/audios/submit" method="post">
    <div class="list-group">
        <?php foreach($audios as $key => $audio): ?>
            <div class="list-group-item-audio clearfix">
                <input type="checkbox" name="audio[]" value="<?= $audio['audio_id']; ?>">
                <audio controls src="/uploads/audios/<?= $audio['f_name']; ?>" height="50px;"></audio>

                <?= $audio['title']; ?>

                <a href="/admin/audios/remove/<?= $audio['name']; ?>" class="btn btn-danger pull-right">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>
                <a href="/admin/audios/edit/<?= $audio['audio_id']; ?>" class="btn btn-default pull-right">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </a>
            </div>
        <?php endforeach;?>
    </div>
</form>