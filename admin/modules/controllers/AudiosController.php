<?php

class AudiosController extends AdminController
{

    public function actionSubmit($parameters = [])
    {
        $audio = new  AudioModel;
        $action = $_POST['action'];
        $array = $_POST;
        $image = false;

        $uploaddir = ROOT . '/../uploads/audios/';
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);
        $filetypes = array(
            'audio/mp3',
            'audio/audio');
            if ($_FILES['file']['size'] > 0 && in_array($_FILES['file']['type'], $filetypes)) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    $image = $_FILES['file']['name'];
                }
            }

        if ($image) {
            $array['f_name'] = $image;//
            $audio->initObjectFromArray($array);

            if ($action == 'edit') {
                $audio->update();
            } elseif ($action == 'create') {
                $audio->create();
            }
            header("Location: /admin/audios");
            die;
        } else {
            header("Location: /admin/audios/add");
            die;
        }


    }

    public function actionView($parameters = [])
    {
        $audio_id = $parameters[0];
        $audio = new Audios($audio_id);
        $params["audio"] = $audio;
        $content = (new View('audios/view', $params))->getHTML();
        $this->view->setParam("title", $audio->name);
        $this->view->setParam("content", $content);
    }

    public function actionEdit($parameters = [])
    {
        $audio_id = $parameters[0];
        $audio = new Audios($audio_id);
        $params["audios"] = $audio;
        $params["action"] = "edit";
        $content = (new View('audios/form', $params))->getHTML();
        $this->view->setParam("name", "Редагування фотографії");
        $this->view->setParam("content", $content);
    }

    public function actionRemove($parameters = [])
    {
        $audio_id = $parameters[0];
        $audio = new Audios($audio_id);

        $audio->remove();

        header("Location: /admin/audios");
        die;
    }

    public function actionAdd($parameters = [])
    {
        $categories = CategoryModel::getCategories();
        $params["categories"] = $categories;
        $params["action"] = "create";
        $content = (new View('audios/form', $params))->getHTML();
        $this->view->setParam("name", "Додавання фотографії");
        $this->view->setParam("content", $content);
    }

    public function actionList($parameters = [])
    {
        $params['audios'] = AudioModel::getAudios();
        $content = (new View('audios/list', $params))->getHTML();
        $this->view->setParam("name", "Список фотографій");
        $this->view->setParam("content", $content);
    }
}
