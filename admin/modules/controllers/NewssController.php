<?php

class PhotosController extends AdminController
{

    public function actionSubmit($parameters = [])
    {
        $photo = new  PhotoModel;
        $action = $_POST['action'];
        $array = $_POST;
        $image = false;

        $uploaddir = ROOT . '/../uploads/photos/';
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);
        $filetypes = array(
            'image/jpeg',
            'image/png',
            'image/jpg');
            if ($_FILES['file']['size'] > 0 && in_array($_FILES['file']['type'], $filetypes)) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    $image = $_FILES['file']['name'];
                }
            }

        if ($image) {
            $array['f_name'] = $image;//
            $photo->initObjectFromArray($array);

            if ($action == 'edit') {
                $photo->update();
            } elseif ($action == 'create') {
                $photo->create();
            }
            header("Location: /admin/photos");
            die;
        } else {
            header("Location: /admin/photos/add");
            die;
        }


    }

    public function actionView($parameters = [])
    {
        $photo_id = $parameters[0];
        $photo = new Photos($photo_id);
        $params["photo"] = $photo;
        $content = (new View('photos/view', $params))->getHTML();
        $this->view->setParam("title", $photo->name);
        $this->view->setParam("content", $content);
    }

    public function actionEdit($parameters = [])
    {
        $photo_id = $parameters[0];
        $photo = new Photos($photo_id);
        $params["photos"] = $photo;
        $params["action"] = "edit";
        $content = (new View('photos/form', $params))->getHTML();
        $this->view->setParam("name", "Редагування фотографії");
        $this->view->setParam("content", $content);
    }

    public function actionRemove($parameters = [])
    {
        $photo_id = $parameters[0];
        $photo = new Photos($photo_id);

        $photo->remove();

        header("Location: /admin/photos");
        die;
    }

    public function actionAdd($parameters = [])
    {
        $categories = CategoryModel::getCategories();
        $params["categories"] = $categories;
        $params["action"] = "create";
        $content = (new View('photos/form', $params))->getHTML();
        $this->view->setParam("name", "Додавання фотографії");
        $this->view->setParam("content", $content);
    }

    public function actionList($parameters = [])
    {
        $params['photos'] = PhotoModel::getPhotos();
        $content = (new View('photos/list', $params))->getHTML();
        $this->view->setParam("name", "Список фотографій");
        $this->view->setParam("content", $content);
    }
}
