<?php
return array(
    ""                  => "home/index",
    "page404"           => "home/pageNotFound",
    "categories"        => "category/list",
    "category/([0-9]+)" => "category/view/$1",
    "products"          => "product/list",
    "products/([0-9]+)" => "product/list/$1",
    "product/([0-9]+)"  => "product/view/$1",
    "product/addToCart" => "product/addToCart",
    "cart"              => "product/viewCart"
);
