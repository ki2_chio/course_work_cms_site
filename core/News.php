<?php
class Photos extends Model
{
    public $photo_id;
    public $name;
    public $f_name;

    public function __construct($photo_id = 0)
    {
        if ($photo_id > 0) {
            $photoArray = $this->getPhotoById($photo_id);
            $this->initObjectFromArray($photoArray);
        }
    }
    public function getPhotoById($photo_id)
    {
        $result = [];
        $sql = "SELECT * FROM photos"
            . " WHERE photo_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$photo_id]);
        return $stmt->fetch();
    }
    public static function getPhotos()
    {
        $st = DataBase::handler()->query("SELECT * FROM photos");
        return $st->fetchAll();
    }
    public function remove()
    {
        unlink(ROOT . '/../uploads/photos/' . $this->f_name);
        $result = [];
        $sql = "DELETE FROM photos"
            . " WHERE photo_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->photo_id]);
        return;
    }
}
