<?php

class Category extends Model
{
    public $category_id;
    public $name;
    public $description;
    public $status;
    public $url;
    public $meta_description;
    public $meta_keywords;
    public $products;
    
    public function __construct($category_id = 0) 
    {
        if ($category_id > 0) {
            $categoryArray = $this->getCategoryById($category_id);
            $this->initObjectFromArray($categoryArray);
        }
    }
    
    public function getCategoryById($category_id) 
    {
        $sql = "SELECT * FROM category WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$category_id]);
        return $stmt->fetch();
    }
    

    /**
     * Отримання списку категорій
    */
    public static function getCategories() 
    {
        $stmt = DataBase::handler()->query("SELECT * FROM category;");
        return $stmt->fetchAll();
    }  
}
