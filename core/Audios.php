<?php

class Audios extends Model
{
    public $audio_id;
    public $name;
    public $f_name;


    public function __construct($audio_id = 0)
    {
        if ($audio_id > 0) {
            $audioArray = $this->getAudioById($audio_id);
            $this->initObjectFromArray($audioArray);
        }
    }

    public function getAudioById($audio_id)
    {
        $result = [];
        $sql = "SELECT * FROM audios"
            . " WHERE audio_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$audio_id]);
        return $stmt->fetch();
    }

    public static function getAudios()
    {
        $st = DataBase::handler()->query("SELECT * FROM audios");
        return $st->fetchAll();
    }

    public function remove()
    {
        unlink(ROOT . '/../uploads/audios/' . $this->f_name);

        $result = [];
        $sql = "DELETE FROM audios"
            . " WHERE audio_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->audio_id]);
        return;
    }
}
