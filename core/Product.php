<?php

class Product extends Model
{
    public $product_id;
    public $name;
    public $description;
    public $category_id;
    public $category_name;
    public $sku;
    public $image;
    public $price;
    public $sort_order;
    public $status;
    public $url;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;
    
    public function __construct($product_id = 0) {
        if ($product_id > 0) {
            $productArray = $this->getProductById($product_id);
            $this->initObjectFromArray($productArray);
        }
    }
    
    public function getProductById($product_id) {
        $result = [];
        $sql = "SELECT a.*, b.name AS category_name FROM product AS a "
                . "LEFT JOIN category AS b "
                . "ON a.category_id=b.category_id "
                . " WHERE a.product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$product_id]);
        return $stmt->fetch();
    }
    
    public static function getProducts($category_id = 0) {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = DataBase::handler()->query("SELECT * FROM product $where;");
        return $st->fetchAll();
    }    
    
}
