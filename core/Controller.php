<?php
class Controller 
{   
    protected $view;
    protected $log;
    
    public function __construct() 
    {
        $params = [];
        $params['categories'] = Category::getCategories();
        $this->view = new View('index', $params);
    }
    
    public function render()
    {
        $this->view->setParam('log', $this->log);
        $this->view->render();
    }
}
