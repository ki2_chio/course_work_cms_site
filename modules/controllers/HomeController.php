<?php
class HomeController extends Controller
{
    
    public function actionIndex($parameters = [])
    {
        $content = (new View('home/home'))->getHTML();
        $this->view->setParam("content", $content);
    }
    
    public function actionPageNotFound($parameters = [])
    {
        $this->view->setParam("title", "Сторінку не знайдено");
        $this->view->setParam("content", "Тут буде шаблон сторінки 404");
    }
}
