<?php

class ProductController extends Controller 
{
    
    public function actionView($parameters) 
    {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $params["product"] = $product;
        $content = (new View('product/view', $params))->getHTML();
        $this->view->setParam("title", $product->name);
        $this->view->setParam("content", $content);
    } 
    
    public function actionList($parameters) 
    {
        $products = ProductModel::getProduct($parameters[0]);
        $params["products"] = $products;
        $content = (new View('product/list', $params))->getHTML();
        $this->view->setParam("title", "Тут буде заголовок списку статей");
        $this->view->setParam("content", $content);       
    }
}
