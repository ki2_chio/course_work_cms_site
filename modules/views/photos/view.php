<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3">
        <div class="list-group">
            <span class="list-group-item list-group-item-success">Категорії</span>
            <?php foreach($categories as $key => $categoryItem): ?>
                <a class="list-group-item <?php if ($categoryItem['category_id'] == $category->category_id): ?>active<?php endif; ?>" href="/category/<?= $categoryItem['category_id']; ?>">
                    <?php echo $categoryItem["name"]; ?>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    <div class="col-xs-12 col-md-8 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1><?= $category->name; ?></h1>
            </div>
            <div class="panel-body">
                <p><?= $category->description; ?></p>

                <div class="list-group">
                    <span class="list-group-item list-group-item-success">Товари даної категорії</span>
                    <?php foreach ($category->products as $product): ?>
                        <a href="/product/<?= $product['product_id']; ?>" class="list-group-item"><?= $product['name']; ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>