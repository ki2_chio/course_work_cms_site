<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= $product->name ?>
            </div>
            <div class="panel-body">
                <h4>Опис</h4>
                <p><?= $product->description; ?></p>
                <div class="list-group">
                    <div class="list-group-item list-group-item-success">Атрибути</div>
                    <div class="list-group-item">
                        <strong>Ціна:</strong> <?= $product->price; ?>
                    </div>
                    <div class="list-group-item">
                        <strong>Категорія:</strong> <?= $product->category_name; ?>
                    </div>
            </div>
        </div>
    </div>
</div>

